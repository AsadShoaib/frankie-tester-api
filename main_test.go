package main

import (
	"log"
	"testing"
)

var payloads = []Payload{

	// CheckType Test Cases
	{
		CheckType:       "COMBO",
		ActivityType:    "SIGNUP",
		CheckSessionKey: "string",
		ActivityData: []ActivityData{
			{"ip.address", "1.23.45.123", "general.integer"},
		}},
	{
		CheckType:       "BIOMETRIC",
		ActivityType:    "SIGNUP",
		CheckSessionKey: "string",
		ActivityData: []ActivityData{
			{"ip.address", "1.23.45.123", "general.integer"},
		}},
	{
		CheckType:       "DEVICE",
		ActivityType:    "SIGNUP",
		CheckSessionKey: "string",
		ActivityData: []ActivityData{
			{"ip.address", "1.23.45.123", "general.integer"},
		}},

	// ActivityType Test Cases

	{
		CheckType:       "COMBO",
		ActivityType:    "SIGNUP",
		CheckSessionKey: "string",
		ActivityData: []ActivityData{
			{"ip.address", "1.23.45.123", "general.integer"},
		}},
	{
		CheckType:       "BIOMETRIC",
		ActivityType:    "LOGIN",
		CheckSessionKey: "string",
		ActivityData: []ActivityData{
			{"ip.address", "1.23.45.123", "general.integer"},
		}},
	{
		CheckType:       "DEVICE",
		ActivityType:    "PAYMENT",
		CheckSessionKey: "string",
		ActivityData: []ActivityData{
			{"ip.address", "1.23.45.123", "general.integer"},
		}},
	{
		CheckType:       "DEVICE",
		ActivityType:    "CONFIRMATION",
		CheckSessionKey: "string",
		ActivityData: []ActivityData{
			{"ip.address", "1.23.45.123", "general.integer"},
		}},

	// ActivityData Test Cases

	{
		CheckType:       "COMBO",
		ActivityType:    "SIGNUP",
		CheckSessionKey: "string",
		ActivityData: []ActivityData{
			{"ip.address", "1.23.45.123", "general.string"},
		}},
	{
		CheckType:       "BIOMETRIC",
		ActivityType:    "LOGIN",
		CheckSessionKey: "string",
		ActivityData: []ActivityData{
			{"ip.address", "1.23.45.123", "general.integer"},
		}},
	{
		CheckType:       "DEVICE",
		ActivityType:    "PAYMENT",
		CheckSessionKey: "string",
		ActivityData: []ActivityData{
			{"ip.address", "1.23.45.123", "general.float"},
		}},
	{
		CheckType:       "DEVICE",
		ActivityType:    "CONFIRMATION",
		CheckSessionKey: "string",
		ActivityData: []ActivityData{
			{"ip.address", "1.23.45.123", "general.bool"},
		}},
}

func TestChecksValidation(t *testing.T) {

	for _, payload := range payloads {
		err := payload.Validate()
		if err != nil {
			log.Println("Failed Test Case -> ", payload)
			t.Fatal(err)
		}
	}

}
