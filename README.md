# Go API Server for swagger
This API allows developers to test the Universal SDK output to ensure it looks right.. ------ The traditional Swagger view of this documentation can be found here:   - https://app.swaggerhub.com/apis-docs/FrankieFinancial/TestUniversalSDK/    

## Overview
I have used Go Package Validator to add validation tags in a Payload structs, predefined validation tags have been used to add common functionality like if field is required, or it should be an ip address. Additionally, I have written a custom validation functions to check the values against enums of CheckType, ActivityType and KVPValue.
Also, I have written test cases in main_test.go file.

### Running the server
To run the server, follow these simple steps:
```
go run main.go
it would run the server at http://localhost:3037
```

### For Test Cases
To run the test cases, use the below command:

```
go test
```
Additionally, you can test the API by using a software like Postman or Insomnia and use this route http://localhost:3037/isgood with Json Payload to check the results

### For Deployment

I haven't written a dockerfile, but this project can be deployed by building a docker image by using a dockerfile then use a docker compose to deploy it on a virtual machine. 
