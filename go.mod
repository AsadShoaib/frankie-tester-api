module go-server-server-generated

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-playground/validator/v10 v10.4.1 // indirect
	github.com/gorilla/mux v1.8.0
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
